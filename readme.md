# Simple oAuth reverse proxy with redirections

Basic jwt authentication for webs and apis.

## UI

Reverse proxy for UI. Sets cookies and does redirects.

UI validation runs on port 8081

## API

Searches for cookie. Fails if not exists.

API validation runs on port 8082

Upstream API must be expecting a custom header (e.g: `X-User-Id`) or a `Authorization` header. Both will be provided with the user id extracted from jwt token `sub` field.

## Plugins

- Auth0
- FusionAuth
