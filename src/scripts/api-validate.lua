local load_common = loadfile('/auth/common/common.lua')
local common = load_common()
local log = common.log

local function return_unauthorized()
    ngx.status = ngx.HTTP_UNAUTHORIZED

    ngx.exit(ngx.HTTP_UNAUTHORIZED )
end

local function get_auth_token_redirect_if_fail()
    local auth_cookie_name = os.getenv('AP_AUTH_COOKIE_NAME')
    local token = ngx.var['cookie_' .. auth_cookie_name]
    if token == nil then
        log.info('Auth token not found')
        return_unauthorized()
    end

    log.debug('Auth token found: `', token, '`')

    return token
end

local function validate_auth_token_return_decoded_payload_redirect_if_fail(token)
    local jwt = require 'resty.jwt'
    -- validate any specific claims you need here
    -- https://github.com/SkyLothar/lua-resty-jwt#jwt-validators
    local validators = require 'resty.jwt-validators'
    local claim_spec = {
        -- validators.set_system_leeway(15), -- time in seconds
        exp = validators.is_not_expired()
        -- iat = validators.is_not_before(),
        -- iss = validators.opt_matches('^http[s]?://yourdomain.auth0.com/$'),
        -- sub = validators.opt_matches('^[0-9]+$')
        -- name = validators.equals_any_of({ 'John Doe', 'Mallory', 'Alice', 'Bob' }),
    }

    local secret = os.getenv('AP_SIGNING_SECRET')
    local jwt_obj = jwt:verify(secret, token, claim_spec)
    if not jwt_obj.verified then
        log.info('Token validation failed: ', jwt_obj.reason)
        return_unauthorized()
    end

    log.debug('Auth token validated')

    return jwt_obj.payload
end

local function massage_http_headers_for_upstream(payload)
    ngx.req.clear_header('Authorization')

    local userId = payload['sub']
    ngx.req.set_header('X-UserId', userId)
    ngx.req.set_header('X-User-Id', userId)
    ngx.req.set_header('Authorization', 'User-Id ' .. userId)
    
    local email = payload['email']
    ngx.req.set_header('X-User-Email', email)
end

local function set_upstream()
    ngx.var.upstream = os.getenv('AP_UPSTREAM')
end

local function log_headers()
    if (common.is_development()) then
        return
    end

    local json = require 'cjson'
    local all_headers = ngx.req.get_headers()
    local all_headers_as_json = json.encode(all_headers)
    log.debug('Http Headers: ' .. all_headers_as_json)
end

log_headers()
set_upstream()
local token = get_auth_token_redirect_if_fail()
local payload = validate_auth_token_return_decoded_payload_redirect_if_fail(token)
massage_http_headers_for_upstream(payload)
