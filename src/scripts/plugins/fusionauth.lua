local json = require 'cjson'
local http = require 'resty.http'

local load_logging = loadfile('/auth/common/logging.lua')
local log = load_logging()

local module = {}

local function get_user_profile(access)

    local get_userinfo_request = os.getenv('AP_USERINFO_URL')

    local httpc = http:new()
    local userinfo_res, userinfo_err = httpc:request_uri(get_userinfo_request, {
        method = 'GET',
        headers = {
            ['Content-Type'] = 'application/x-www-form-urlencoded',
            ['Authorization'] = 'Bearer ' .. access
        },
        keepalive_timeout = 60,
        keepalive_pool = 10
    })

    if not userinfo_res or userinfo_res.status ~= ngx.HTTP_OK then
        return ''
    end

    local body = userinfo_res.body

    log.debug('User info: ' .. body)

    local escaped_jsoned_profile = ngx.escape_uri(body)

    return escaped_jsoned_profile

end

function module.get_tokens_fail_if_something_fails(code)
    local get_token_request = os.getenv('AP_TOKENS_URL')

    local client_id = os.getenv('AP_CLIENT_ID')
    local secret = os.getenv('AP_SECRET')
    local callback_uri = os.getenv('AP_CALLBACK')
    local url_encoded_callback = ngx.escape_uri(callback_uri)

    local url_encoded_payload = string.format(
        'client_id=%s&code=%s&client_secret=%s&redirect_uri=%s&grant_type=authorization_code',
        client_id,
        code,
        secret,
        url_encoded_callback)

    log.debug('Getting tokens...')
    log.debug('    get_token_request: ' .. get_token_request)
    log.debug('    url_encoded_payload: ' .. url_encoded_payload)

    local httpc = http:new()
    local res, err = httpc:request_uri(get_token_request, {
        method = 'POST',
        body = url_encoded_payload,
        headers = {
            ['Content-Type'] = 'application/x-www-form-urlencoded',
        },
        keepalive_timeout = 60,
        keepalive_pool = 10
    })

    if not res or res.status ~= ngx.HTTP_OK then
        local actual_res = res or {}
        status_code = actual_res.status or 'unknown'
        error = err or 'unknown'

        log.warn('Failed getting tokens:' .. res.body)
        log.warn('status code: ' .. status_code)
        log.warn('error: ' .. error)

        ngx.status = ngx.HTTP_UNAUTHORIZED
        ngx.say('Could not authenticate')
        ngx.exit(ngx.HTTP_UNAUTHORIZED)

        return nil
    end

    local body = res.body

    local decoded = json.decode(body)
    local access = decoded.access_token

    local profile = get_user_profile(access)

    log.debug('Got tokens')
    log.debug('    access_token: ' .. access)
    log.debug('    profile: ' .. profile)

    local tokens = {
        profile = profile,
        access = access
    }

    return tokens
end

return module