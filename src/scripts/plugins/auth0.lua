local http = require 'resty.http'
local jwt = require 'resty.jwt'
local json = require 'cjson'

local load_logging = loadfile('/auth/common/logging.lua')
local log = load_logging()

local module = {}

function module.get_tokens_fail_if_something_fails(code)
    local get_token_request = os.getenv('AP_TOKENS_URL')

    local client_id = os.getenv('AP_CLIENT_ID')
    local secret = os.getenv('AP_SECRET')
    local callback_uri = os.getenv('AP_CALLBACK')

    local payload = {
        grant_type = 'authorization_code',
        code = code,
        client_id = client_id,
        client_secret = secret,
        redirect_uri = callback_uri
    }
    local json_payload = json.encode(payload)

    log.debug('Getting tokens...')
    log.debug('    get_token_request: ' .. get_token_request)
    log.debug('    json_payload: ' .. json_payload)

    local httpc = http:new()
    local res, err = httpc:request_uri(get_token_request, {
        method = 'POST',
        body = json_payload,
        headers = {
            ['Content-Type'] = 'application/json',
        },
        keepalive_timeout = 60,
        keepalive_pool = 10
    })

    if not res or res.status ~= ngx.HTTP_OK then
        local actual_res = res or {}
        status_code = actual_res.status or 'unknown'
        error = err or 'unknown'

        log.warn('Failed getting tokens:')
        log.warn('status code: ' .. status_code)
        log.warn('error: ' .. error)

        ngx.status = ngx.HTTP_UNAUTHORIZED
        ngx.say('Could not authenticate')
        ngx.exit(ngx.HTTP_UNAUTHORIZED)

        return nil
    end

    local body = res.body
    log.debug('Got tokens') 
    log.debug('Encoded: ' .. body)

    local decoded = json.decode(body)
    log.debug('    id_token: ' .. decoded.id_token)
    log.debug('    access_token: ' .. decoded.access_token)

    local loaded_token = jwt:load_jwt(decoded.id_token)
    local profile = loaded_token.payload
    local jsoned_profile = json.encode(profile)
    local escaped_jsoned_profile = ngx.escape_uri(jsoned_profile)

    local tokens = {
        profile = escaped_jsoned_profile,
        access = decoded.access_token
    }

    return tokens
end

return module