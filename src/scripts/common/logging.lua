local module = {}

local function log(level, message)
    ngx.log(level, message)
end

function module.info(message)
    log(ngx.INFO, message)
end

function module.error(message)
    log(ngx.ERR, message)
end

function module.warn(message)
    log(ngx.WARN, message)
end

function module.debug(message)
    log(ngx.DEBUG, message)
end

return module