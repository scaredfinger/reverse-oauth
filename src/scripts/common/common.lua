local module = {}

local function load_submodule(path)

    local load_submodule_actual = loadfile('/auth/common/logging.lua')
    local submodule = load_submodule_actual()
    return submodule

end

function module.is_development()
    return os.getenv('AP_DEVELOPMENT') == 'true'
end

module.log = load_submodule('/auth/common/logging.lua')

return module