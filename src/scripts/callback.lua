local resty_url = require 'resty.url'

local load_common = loadfile('/auth/common/common.lua')
local common = load_common()
local log = common.log

local plugin_name = os.getenv('AP_PLUGIN')
local load_plugin = assert(loadfile('/auth/plugins/' .. plugin_name .. '.lua'))
local auth = load_plugin()

local function build_cookie(name, value, domain)
    if (domain == nil
        or domain == ''
        or string.match(domain, 'localhost'))
    then
        return string.format(
            '%s=%s',
            name,
            value)
    else
        return string.format(
            '%s=%s; Domain=%s',
            name,
            value,
            domain)
    end
end

local function get_code_fail_if_not_found()
    local url = require 'net.url'
    local request_uri = ngx.var.request_uri
    local parsed = url.parse(request_uri)
    local code = parsed.query.code

    if code == nil then
        log.warn('Could not find any code')
        ngx.exit(ngx.HTTP_BAD_REQUEST)
    end

    return code
end

local function get_tokens_fail_if_something_fails(code)
    return auth.get_tokens_fail_if_something_fails(code)
end

local function get_cookies()
    local cookies = ngx.header['Set-Cookie'] or {}

    if type(cookies) == 'string'
    then
        cookies = {cookies}
    end

    return cookies
end

local function add_cookie(cookie)
    local cookies = get_cookies()

    local cookie_text = build_cookie(
        cookie.name,
        cookie.value,
        cookie.domain)

    table.insert(cookies, cookie_text)

    ngx.header['Set-Cookie'] = cookies
end

local function set_auth_cookie(token)
    local auth_cookie_name = os.getenv('AP_AUTH_COOKIE_NAME')
    local auth_cookie_domain = os.getenv('AP_AUTH_COOKIE_DOMAIN')

    add_cookie({
        name = auth_cookie_name,
        value = token,
        domain = auth_cookie_domain
    })

    log.debug('Auth cookie set: ' .. auth_cookie_name .. '=' .. token)
end

local function set_profile_cookie(escaped_jsoned_profile)
    local profile_cookie_name = os.getenv('AP_PROFILE_COOKIE_NAME')
    local profile_cookie_domain = os.getenv('AP_PROFILE_COOKIE_DOMAIN')

    add_cookie({
        name = profile_cookie_name,
        value = escaped_jsoned_profile,
        domain = profile_cookie_domain
    })

    log.debug('Profile cookie set: ' .. profile_cookie_name .. '=' .. escaped_jsoned_profile)
end

local function ends_with(str, ending)
    return ending == '' or str:sub(-#ending) == ending
end

local function starts_with(str, start)
    return start == '' or str:sub(#start) == start
end

local function redirect()
    ngx.status = ngx.HTTP_MOVED_TEMPORARILY

    local redirect_cookie_name = os.getenv('AP_REDIRECT_COOKIE_NAME')
    local AP_APP_BASE = os.getenv('AP_APP_BASE')
    local escaped_redirect_uri = ngx.var['cookie_' .. redirect_cookie_name]
    local redirect_uri = ngx.unescape_uri(escaped_redirect_uri)

    if ends_with(redirect_uri, '.js') or ends_with(redirect_uri, '.css')
    then
        redirect_uri = '/'
    end

    if  not starts_with(redirect_uri, AP_APP_BASE)
    then
        redirect_uri = resty_url.join(AP_APP_BASE, redirect_uri)
    end

    ngx.header.Location = redirect_uri

    log.debug('Redirection header set: Location=' .. redirect_uri)
    ngx.exit(ngx.HTTP_MOVED_TEMPORARILY)
end

local function log_entry()

    local request_uri = ngx.var.uri
    log.info('Request: `', request_uri, '`')

    log.debug('Cookies:')
    for key, value in pairs(ngx.var) do
        if (string.match(key, 'cookie_'))
        then
            log.debug(key .. '=' .. value)
        end
    end

    log.debug('Http Headers:')
    for key, value in pairs(ngx.req.get_headers()) do
        log.debug(key .. ':' .. value)
    end

end

-- Main
log_entry()
local code = get_code_fail_if_not_found()
local tokens = get_tokens_fail_if_something_fails(code)

if (tokens == nil) then
    return
end

set_profile_cookie(tokens.profile)
set_auth_cookie(tokens.access)
redirect()