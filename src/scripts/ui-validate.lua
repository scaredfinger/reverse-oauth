local load_common = loadfile('/auth/common/common.lua')
local common = load_common()
local log = common.log

local function set_cookie(name, value, domain)
    if (domain == nil
        or domain == ''
        or string.match(domain, 'localhost'))
    then
        log.debug('Setting localhost cookie')
        ngx.header['Set-Cookie'] = string.format(
            '%s=%s; Path=/',
            name,
            value)
    else
        log.debug(ngx.DEBUG, 'Setting cookie for domain')
        ngx.header['Set-Cookie'] = string.format(
            '%s=%s; Domain=%s; Path=/',
            name,
            value,
            domain)
    end
end

local function get_location()

    local login_url = os.getenv('AP_LOGIN_URL')
    local client = os.getenv('AP_CLIENT_ID')
    local callback = os.getenv('AP_CALLBACK')
    local url_encoded_callback = ngx.escape_uri(callback)

    local uri =
        string.format(
        login_url,
        client,
        url_encoded_callback)

    return uri
end

local function redirect_to_auth_page()
    ngx.status = ngx.HTTP_MOVED_TEMPORARILY
    local request_uri = ngx.var.uri or '/'
    local escaped_request_uri = ngx.escape_uri(request_uri)
    local redirect_cookie_name = os.getenv('AP_REDIRECT_COOKIE_NAME')
    local redirect_cookie_domain = os.getenv('AP_REDIRECT_COOKIE_DOMAIN')

    set_cookie(redirect_cookie_name, escaped_request_uri, redirect_cookie_domain)

    log.debug('Setting redirection to: `', request_uri, '`')

    ngx.header.Location = get_location()

    ngx.exit(ngx.HTTP_MOVED_TEMPORARILY)
end

local function get_auth_token_redirect_if_fail()
    local auth_cookie_name = os.getenv('AP_AUTH_COOKIE_NAME')
    local token = ngx.var['cookie_' .. auth_cookie_name]
    if token == nil then
        log.info('Auth token not found. Redirecting to authorize page')
        redirect_to_auth_page()
    end

    log.debug('Auth token found: `', token, '`')

    return token
end

local function validate_auth_token_return_decoded_payload_redirect_if_fail(token)
    local jwt = require 'resty.jwt'
    -- validate any specific claims you need here
    -- https://github.com/SkyLothar/lua-resty-jwt#jwt-validators
    local validators = require 'resty.jwt-validators'
    local claim_spec = {
        -- validators.set_system_leeway(15), -- time in seconds
        exp = validators.is_not_expired()
        -- iat = validators.is_not_before(),
        -- iss = validators.opt_matches('^http[s]?://yourdomain.auth0.com/$'),
        -- sub = validators.opt_matches('^[0-9]+$')
        -- name = validators.equals_any_of({ 'John Doe', 'Mallory', 'Alice', 'Bob' }),
    }

    local secret = os.getenv('AP_SIGNING_SECRET')
    local jwt_obj = jwt:verify(secret, token, claim_spec)
    if not jwt_obj.verified then
        log.info('Token validation failed: `', jwt_obj.reason, '`. Redirecting to authorize page')
        redirect_to_auth_page()
    end

    return jwt_obj.payload
end

local function massage_http_headers_for_upstream(payload)
    ngx.req.clear_header('Authorization')

    local userId = payload['sub']
    ngx.req.set_header('X-UserId', userId)
    ngx.req.set_header('X-User-Id', userId)
    
    local email = payload['email']
    ngx.req.set_header('X-User-Email', email)
end

local function set_upstream()
    ngx.var.upstream = os.getenv('AP_UPSTREAM')
end

local request_uri = ngx.var.uri
log.info('Request: `', request_uri, '`')
set_upstream()
local token = get_auth_token_redirect_if_fail()
local payload = validate_auth_token_return_decoded_payload_redirect_if_fail(token)
massage_http_headers_for_upstream(payload)
