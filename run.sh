PLUGIN=${1:-fusionauth}

echo Loading ./load-secrets-${PLUGIN}.sh
source ./load-secrets-${PLUGIN}.sh

docker-compose up \
    --detach \
    --build \
    --remove-orphans
